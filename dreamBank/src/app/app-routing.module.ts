import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './Components/login/login.component';
import { AccoutDetailsComponent } from './Components/accout-details/accout-details.component';
import { NewProductComponent } from './Components/new-product/new-product.component';
import { NewProductSuccessComponent } from './Components/new-product-success/new-product-success.component';
import { HomeComponent } from './Components/home/home.component';


const routes: Routes = [
  { path: '', component: LoginComponent },
  { path: 'login',  component: LoginComponent },
  { path: 'home',  component: HomeComponent },
  { path: 'newProduct',  component: NewProductComponent },
  { path: 'newProductSuccess',  component: NewProductSuccessComponent },
  { path: 'accountDetails',  component: AccoutDetailsComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
