import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './Components/login/login.component';
import { HomeComponent } from './Components/home/home.component';
import { AccoutDetailsComponent } from './Components/accout-details/accout-details.component';
import { NewProductSuccessComponent } from './Components/new-product-success/new-product-success.component';
import { NewProductComponent } from './Components/new-product/new-product.component';
import { NavbarComponent } from './Components/navbar/navbar.component';
import { HeaderComponent } from './Components/header/header.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CardsComponent } from './Components/cards/cards.component';
import { FooterComponent } from './Components/footer/footer.component';
import { BudgetComponent } from './Components/budget/budget.component';

import { Globals } from './Globals/globals';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule} from "@angular/forms";
import { GraphComponent } from './Components/graph/graph.component';
import { LoaderComponent } from './Components/loader/loader.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    AccoutDetailsComponent,
    NewProductSuccessComponent,
    NewProductComponent,
    NavbarComponent,
    HeaderComponent,
    CardsComponent,
    FooterComponent,
    BudgetComponent,
    GraphComponent,
    LoaderComponent    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [Globals],
  bootstrap: [AppComponent]
})
export class AppModule { }
