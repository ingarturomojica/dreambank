import { Component,  OnInit  } from '@angular/core';
import { UserService } from './../../Service/User/user.service';
import { User } from './../../Interfase/User';
import { Globals } from './../../Globals/globals';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit  {
  userLocal:User = {}
  isDisableBtn:boolean = true;
  isMsgError:boolean = false;

  constructor(private userService:UserService,private router:Router, private globals:Globals) { }
  
  ngOnInit(): void {

  }

  validForm(){
    this.isMsgError=false;    
    if(this.userLocal.login !== undefined && this.userLocal.login !== '' &&
       this.userLocal.password !== undefined && this.userLocal.password !== ''){
      this.isDisableBtn = false;
    }else{
      this.isDisableBtn = true;      
    }
  }

  login(){
    this.userService.getUsers().subscribe(data =>{

      this.globals.userLogin = data.filter(userBD => userBD.login === this.userLocal.login &&
        userBD.password=== this.userLocal.password)[0];

      if ( this.globals.userLogin !== undefined){
          this.globals.loader = false;
          this.router.navigate(['/home']);
      }else{
        this.isMsgError=true;
      }
    },error =>{
      return null;
    })
  }

}
