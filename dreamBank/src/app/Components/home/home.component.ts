import { Component, OnInit } from '@angular/core';
import { Globals } from './../../Globals/globals';
import { AccountService } from './../../Service/Account/account.service';
import { ProductService } from './../../Service/Product/product.service';
import { Account } from './../../Interfase/Account';
import { Product } from './../../Interfase/Product';
import {Router} from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  accounts:Account[] = [];
  products:Product[] = [];



  constructor(public globals:Globals, private router:Router, 
              private accountService:AccountService,
              private productService:ProductService) { }

  ngOnInit(): void {
    if(this.globals.isLogin() === false){this.router.navigate(['/'])};    
    this.getAccount();
    this.getProducts();
  }

  getAccount(){
    this.accountService.getAccount(this.globals.userLogin.login!).subscribe(data=>{
      this.accounts = data;
      this.accounts = data.filter(accountActualy => accountActualy.idUser == this.globals.userLogin.idUser);
      this.globals.loader = false;
    },error =>{
      console.log("error",error);
    });
  }

  getProducts(){
    this.productService.getProductsidUser(this.globals.userLogin.idUser!).subscribe(data=>{
      this.products = data.filter(productActually => productActually.idUser == this.globals.userLogin.idUser);
      this.globals.loader = false;
    },error =>{
      console.log("error",error);
    });
  }

  accountDetail(account:Account){
    this.globals.accountSelect = account;
    this.router.navigate(['/accountDetails']);
  }

}
