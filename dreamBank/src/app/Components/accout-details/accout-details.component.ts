import { Component, OnInit } from '@angular/core';
import { Globals } from './../../Globals/globals';
import {Router} from '@angular/router';

import { TransactionService } from './../../Service/Transaction/transaction.service';
import { Transaction } from './../../Interfase/Transaction';


@Component({
  selector: 'app-accout-details',
  templateUrl: './accout-details.component.html',
  styleUrls: ['./accout-details.component.css']
})
export class AccoutDetailsComponent implements OnInit {

  transactions:Transaction[] = [];

  constructor(public globals:Globals, private router:Router, private transactionService:TransactionService) { }

  ngOnInit(): void {
    if(this.globals.isLogin() === false){this.router.navigate(['/'])};    
    this.getTransactions();
    
  }

  backHome(){
    this.globals.accountSelect = {};
    this.router.navigate(['/home']);
  }
  getTransactions(){
    this.transactionService.getTransactionIdAccount(this.globals.accountSelect.idAccount!).subscribe(data=>{
      this.transactions = data.filter(transactionBD => transactionBD.idAccount === this.globals.accountSelect.idAccount);
      this.globals.loader = false;
    },error =>{
      console.log("error",error);
    })
  }

}
