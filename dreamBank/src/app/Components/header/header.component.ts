import { Component, OnInit } from '@angular/core';
import { Globals } from './../../Globals/globals';
import {Router} from '@angular/router';
import { AccountService } from './../../Service/Account/account.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  totalBalance:number =0;

  constructor(public globals:Globals, private router:Router, private accountService:AccountService) { }

  ngOnInit(): void {
    this.accountService.getAccountBalanceTotal(this.globals.userLogin.login!).subscribe(data=>{
      this.totalBalance = data.totalBalance;
      this.globals.loader = false;
    },error =>{
      console.log("error",error);
    });
  }

  logout($event:any){
    if ($event.target.value === "logout"){
      this.globals.logout();
      this.router.navigate(['/']);    
    };
  
  }

  goToNewProduct(){
    this.router.navigate(['/newProduct']);  
  }
}
