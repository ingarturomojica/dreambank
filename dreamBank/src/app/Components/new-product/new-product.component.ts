import { Component, OnInit } from '@angular/core';
import { Globals } from './../../Globals/globals';
import {Router} from '@angular/router';

@Component({
  selector: 'app-new-product',
  templateUrl: './new-product.component.html',
  styleUrls: ['./new-product.component.css']
})
export class NewProductComponent implements OnInit {

  constructor(private globals:Globals, private router:Router) { }

  ngOnInit(): void {
    if(this.globals.isLogin() === false){this.router.navigate(['/'])};    
  }

  backHome(){
    this.router.navigate(['/home']);  
  }
  goToProductSuccess(){
    this.router.navigate(['/newProductSuccess']);  
  }

  

}
