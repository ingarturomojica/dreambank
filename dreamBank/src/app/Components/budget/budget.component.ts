import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-budget',
  templateUrl: './budget.component.html',
  styleUrls: ['./budget.component.css']
})
export class BudgetComponent implements OnInit {

  @Input('percentage') percentage: string = "";

  constructor() { }

  ngOnInit(): void {
    setTimeout(() => {
      this.tour();
    }, 1000);
   
  }

  tour() {

    var arcos = document.querySelectorAll("#budget__graph .bow"); 
    
    let elem = 0; 
    
    while(elem < arcos.length) {
    var identificador = "e13m3nt0"+elem; 
    var grados = (parseInt(arcos[elem].innerHTML) / 100) * 180; 
    
    document.styleSheets[0].insertRule("#"+ identificador +"::before {transition: transform 3s cubic-bezier(.3,.2,.45,1.5) 1s; transform: rotate("+ grados +"deg)}", document.styleSheets[0].cssRules.length); 
    
    arcos[elem].setAttribute("id",identificador); 
    
    elem++; 
    }; 
    
    }

}
