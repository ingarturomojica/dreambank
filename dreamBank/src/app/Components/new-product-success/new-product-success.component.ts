import { Component, OnInit } from '@angular/core';
import { Globals } from './../../Globals/globals';
import {Router} from '@angular/router';

@Component({
  selector: 'app-new-product-success',
  templateUrl: './new-product-success.component.html',
  styleUrls: ['./new-product-success.component.css']
})
export class NewProductSuccessComponent implements OnInit {

  
  constructor(private globals:Globals, private router:Router) { }

  ngOnInit(): void {
    if(this.globals.isLogin() === false){this.router.navigate(['/'])};    
  }

  backHome(){
    this.router.navigate(['/home']);  
  }

}
