import { Component, OnInit,Input } from '@angular/core';
import { Product } from './../../Interfase/Product';

@Component({
  selector: 'app-cards',
  templateUrl: './cards.component.html',
  styleUrls: ['./cards.component.css']
})
export class CardsComponent implements OnInit {
  @Input('product') product: Product = {};

  constructor() { }

  ngOnInit(): void {
  }

}
