import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { Globals } from './../../Globals/globals';
import { User } from './../../Interfase/User';

@Injectable({
  providedIn: 'root'
})

export class UserService {

  constructor(public http:HttpClient,public globals:Globals) { }

  getUsers(){    
    this.globals.loader = true;
    return this.http.get<User[]>(this.globals.getAPI()+'users');
  }
  
}
