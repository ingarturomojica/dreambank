import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { Globals } from './../../Globals/globals';
import { Account } from './../../Interfase/Account';


@Injectable({
  providedIn: 'root'
})
export class AccountService {

  
  constructor(public http:HttpClient,public globals:Globals) { }

  getAccount(login:string){  
    this.globals.loader = true;     
    let params = {login:login};
    return this.http.post<Account[]>(this.globals.getAPI()+'account',params);
  }

  getAccountBalanceTotal(login:string){   
    this.globals.loader = true;    
    let params = {login:login};
    return this.http.post<any>(this.globals.getAPI()+'accountTotalBalance',params);
  }
}
