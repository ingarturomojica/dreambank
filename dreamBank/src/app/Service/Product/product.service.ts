import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { Globals } from './../../Globals/globals';
import { Product } from './../../Interfase/Product';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(public http:HttpClient,public globals:Globals) { }

  getProductsidUser(idUser:string){ 
    this.globals.loader = true;
    let params = {"idUser": idUser}   
    return this.http.post<Product[]>(this.globals.getAPI()+'products',params);
  }
}
