import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { Globals } from './../../Globals/globals';
import { Transaction } from './../../Interfase/Transaction';

@Injectable({
  providedIn: 'root'
})
export class TransactionService {

  constructor(public http:HttpClient,public globals:Globals) { }

  getTransactionIdAccount(idAccount:string){ 
    this.globals.loader = true;
    let params = {"idAccount": idAccount}   
    return this.http.post<Transaction[]>(this.globals.getAPI()+'accountDetails',params);
  }
  
}
