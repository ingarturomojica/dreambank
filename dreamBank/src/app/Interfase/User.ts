export interface User {
    idUser?:string
    login?:string
    password?:string
    firstName?:string
    lastName?:string
    lastLogin?:string
    picture?:string    
  };
  