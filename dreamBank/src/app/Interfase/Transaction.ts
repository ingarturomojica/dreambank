export interface Transaction {
    idAccount?:string
    date?:string,
    description?:string
    currency?:string
    value?:string
    balance?:string
  };
  