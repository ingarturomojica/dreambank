export interface Account {
    idUser?:string,
    idAccount?:string,
    name?:string
    status?:string
    currency?:string
    balance?:string
    type?:string
  };
  