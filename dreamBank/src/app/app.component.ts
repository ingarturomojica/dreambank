import { Component, OnInit } from '@angular/core';
import { Globals } from './Globals/globals';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'dreamBank';

  constructor(public globals:Globals){}
  
  ngOnInit(): void {

  }

}
