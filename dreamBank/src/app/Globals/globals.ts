import { Injectable } from '@angular/core';
import { User } from './../Interfase/User';
import { Account } from './../Interfase/Account';

@Injectable({
    providedIn: 'root'
})



export class Globals {
  
    //API:string="https://demo5258218.mockable.io/";
    API:string="https://80a5095e-4b69-4fdc-8847-0c68069a1879.mock.pstmn.io/";

    userLogin:User = {};
    accountSelect:Account = {};

    loader:boolean=false;
    error:boolean=false;
    success:boolean=false;

    getAPI(){   
        return this.API;
    }

    logout(){
       this.userLogin = {};
    }
    
    isLogin():boolean{
        return this.userLogin.login === undefined ? false : true;
    }
    
}
