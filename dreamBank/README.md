# DreamBank

Esta aplicación se realizo con [Angular CLI](https://github.com/angular/angular-cli) version 11.0.4.

## Mock Servers

Existen como falsas API dos fuentes de información publicadas por temas de estabilidad, ya que en la primera implementación se observo fallas continuas en https://www.mockable.io/ por lo cual se migro a https://www.postman.com/ quedando con las URL de acceso: 

https://demo5258218.mockable.io/
https://80a5095e-4b69-4fdc-8847-0c68069a1879.mock.pstmn.io/

Cuyos metodos a recuperar para cada una son: 

POST - account: Obtiene las cuentas de un cliente en particular 
POST - accountTotalBalance: Obtiene el total del balance de un cliente en particular
POST - products: Obtiene los diferentes productos e un cliente en particular
POST - accountDetails: Obtiene el detalle de una cuenta en particular
GET - users: Obtiene los usuarios del sistema

## Arquitectura Software

Este proyecto se realizo basado en la aquitectura MVC.

Construyendo el siguiente esquema de carpetas: 

* Class: Ubicar las diferentes clases que se necesitarian para el proyecto
* Components: Ubicar los diferentes componentes realizados para la aplicación que se encontraran  en carpetas independitente nombrados por su mismo nombre y que dentro de esta estara los estilos y logica de cada uno.
* Containers: Ubicar los layout de la aplicación.
* Globals: Ubicar los archivos que contengan variables de acceso globales.
* Interfase: Ubicar las diferentes interfase que se implementarian en la aplicación.
* Loaders: Ubicar los diferentes componentes exclusivo de carga
* Service: Ubicar los diferentes modelos que realizan las conexiones a las API

## Modules Install

Favor antes de realizar la ejecución correr el comando `npm install` para mantener el proyecto con los modulos que se requieren como los siguientes: 

* Boostrap


## test data

Como datos de prueba se realizaron dos usuarios falsos con los siguientes accesos: 

Usuario 1: 
login: 123456
password: 123456

Usuario 2: 
login: 1234567
password: 1234567

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
